package it.unibo.as.logicagents;

import alice.tuprolog.Number;
import alice.tuprolog.Prolog;
import alice.tuprolog.SolveInfo;

/**
 * An artefact aimed at observing the temperature of the environment it is situated into
 */
public class TemperatureSensor extends Artefact {

    /**
     * Creates a new {@link TemperatureSensor} situated into the provided <code>environment</code>
     * @param environment a {@link Prolog} instance acting as environment
     */
    public TemperatureSensor(Prolog environment) {
        super(environment);
    }

    /**
     * Retrieves the temperature of the environment this artefact is situated into
     * @return a <code>int</code> representing the current temperature in the environment
     * @throws Exception if some bug is present (no exception should ever be thrown)
     */
    public int getTemperature() throws Exception {
        SolveInfo query = getEnvironment().solve("temp(X).");
        if (query.isSuccess()) {
            Number temperature = (Number) query.getTerm("X");
            log("Perceiving temperature: %s °C\n", temperature.intValue());
            return temperature.intValue();
        } else {
            return -273; // 0 K
        }
    }
}
