package it.unibo.as.logicagents;

import alice.tuprolog.Prolog;
import alice.tuprolog.Terms;
import alice.tuprolog.exceptions.InvalidObjectIdException;
import alice.tuprolog.lib.OOLibrary;

import java.util.Objects;

/**
 * Base type for all artifacts
 */
public class Artefact {

    private final Prolog environment;

    protected Artefact(Prolog environment) {
        this.environment = Objects.requireNonNull(environment);
    }

    /**
     * Retrieves a reference to the environment this artefact is situated into
     * @return a {@link Prolog} object
     */
    public Prolog getEnvironment() {
        return environment;
    }

    /**
     * Attach this artifact to an agent
     * @param agent the agent this artifact must be attached to (it must be a {@link Prolog} instace which loaded a {@link OOLibrary} instance)
     * @param name the name of the atom this artifact should be accessible by, within <code>agent</code>'s theory
     * @throws InvalidObjectIdException in case <code>name</code> is not a valid atom
     */
    public void attachTo(Prolog agent, String name) throws InvalidObjectIdException {
        final OOLibrary library = (OOLibrary) agent.getLibrary(OOLibrary.class.getName());
        library.register(Terms.atom(Objects.requireNonNull(name)), this);
    }

    protected void log(String format, Object... args) {
        getEnvironment().stdOutput(String.format(format, args));
    }
}
