package it.unibo.as.logicagents;

import alice.tuprolog.Prolog;

/**
 * An artefact aimed at altering the temperature into the environment it is situated into
 */
public class TemperatureActuator extends Artefact {

    /**
     * Creates a new {@link TemperatureActuator} situated into the provided <code>environment</code>
     * @param environment a {@link Prolog} instance acting as environment
     */
    public TemperatureActuator(Prolog environment) {
        super(environment);
    }

    /**
     * Sets the temperature of the environment this artefact is situated into to the provided <code>value</code>
     * @param value is the target temperature value
     * @return <code>true</code> if the temperature has been correctly set, <code>false</code> otherwise
     * @throws Exception if some bug is present (no exception should ever be thrown)
     */
    public boolean setTemperature(int value) throws Exception {
        // TODO implement this
        throw new RuntimeException("not implemented");
    }
}
