package it.unibo.as.logicagents.impl;

import it.unibo.as.logicagents.*;

/**
 * This is both a sensor and an actuator artifact.
 * It lets an agent mode awhile measuring how much it moved since the last it checked
 */
public class MotorWithOdometerImpl extends AbstractArtefact<VacuumEnvironment> implements MotorWithOdometer {

    private int steps = 0;

    public MotorWithOdometerImpl(VacuumEnvironment environment) {
        super(environment);
    }

    @Override
    public boolean move(String directionName) throws Exception {
        final Direction direction = Direction.valueOf(directionName.toUpperCase());
        final Orientation orientation = getEnvironment().getVacuumOrientation().rotate(direction);
        final Vector2D current = getEnvironment().getVacuumPosition();
        final Vector2D next = current.inc(orientation);

        getEnvironment().setVacuumOrientation(orientation);

        if (getEnvironment().isInside(next)) {
            getEnvironment().setVacuumPosition(next);
            steps++;
        }

        return true;
    }

    @Override
    public int countSteps() throws Exception {
        final int result = steps;
        steps = 0;
        return result;
    }
}
