package it.unibo.as.logicagents;

import alice.tuprolog.Prolog;
import alice.tuprolog.Theory;
import alice.tuprolog.event.ExceptionEvent;
import alice.tuprolog.event.OutputEvent;
import alice.tuprolog.exceptions.InvalidObjectIdException;
import alice.tuprolog.exceptions.InvalidTheoryException;
import alice.tuprolog.exceptions.MalformedGoalException;

import java.io.IOException;
import java.io.InputStream;
import java.util.stream.IntStream;

public class Main {

    private static int nFrames;
    private static long fps = 1;

    public static void main(String[] args) throws InvalidTheoryException, MalformedGoalException, IOException, InvalidObjectIdException {
        if (args.length >= 1) {
            fps = Long.parseLong(args[0]);
        }

        Prolog agent = createEngineWithTheory(
                Main.class.getResourceAsStream("/Vacuum.pl"),
                Main.class.getResourceAsStream("/Utils.pl")
        );

        VacuumEnvironment environment = VacuumEnvironment.createWithRandomRobotPose(20, 10);

        environment.onInit(env -> {
            IntStream.range(0, env.getHeight() * env.getWidth()).forEach(i -> env.setRandomlyDirty());
        });

        environment.onVacuumMoved(env -> {
            System.out.printf("### Frame %d ############\n", nFrames++);
            System.out.println(env.toString());
            try {
                Thread.sleep(1000 / fps);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        });

        Artefact<VacuumEnvironment> motor = MotorWithOdometer.create(environment);
        Artefact<VacuumEnvironment> scanner = DirtyScanner.create(environment);
        Artefact<VacuumEnvironment> sucker = Sucker.create(environment);

        motor.attachTo(agent, "motor");
        scanner.attachTo(agent, "scanner");
        sucker.attachTo(agent, "sucker");

        environment.initialize();

        agent.solve("start");
    }

    public static Prolog createEngineWithTheory(InputStream theory, InputStream... otherTheories) throws InvalidTheoryException, IOException {
        Prolog engine = new Prolog();
        final Theory fullTheory = Theory.parseLazilyWithOperators(theory, engine.getOperatorManager());
        for (InputStream tStream : otherTheories) {
            final Theory t = Theory.parseLazilyWithOperators(tStream, engine.getOperatorManager());
            fullTheory.append(t);
        }
        engine.setTheory(fullTheory);
        engine.addOutputListener(Main::printOutput);
        engine.addExceptionListener(Main::printError);
        return engine;
    }


    private static void printOutput(OutputEvent object) {
        System.out.print(cleanUpMessage(object.getMsg()));
    }

    private static void printError(ExceptionEvent object) {
        System.err.print(cleanUpMessage(object.getMsg()));
    }

    private static String cleanUpMessage(String msg) {
        if (msg.startsWith("'") && msg.endsWith("'")) {
            return msg.substring(1, msg.length() - 1);
        }
        return msg;
    }
}
