package it.unibo.as.logicagents;

import it.unibo.as.logicagents.impl.SuckerImpl;

/**
 * This is an actuator artifact letting the agent clean up the environment position it is lying upon
 */
public interface Sucker extends Artefact<VacuumEnvironment> {

    static Sucker create(VacuumEnvironment environment) {
        return new SuckerImpl(environment);
    }

    /**
     * Tries to clean up the environment position the agent is lying upon.
     * Fails in case that position is still dirty after the cleaning attempt
     *
     * @return a <code>boolean</code> value stating whether the cleaning action was successful
     * @throws Exception
     */
    boolean cleanUp() throws Exception;
}
