package it.unibo.as.logicagents.impl;

import alice.tuprolog.Prolog;
import it.unibo.as.logicagents.Orientation;
import it.unibo.as.logicagents.VacuumEnvironment;
import it.unibo.as.logicagents.Vector2D;

import java.util.*;
import java.util.function.Consumer;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class VacuumEnvironmentImpl implements VacuumEnvironment {

    private static final char EMPTY = '_';
    private static final char DIRTY = 'o';

    private final Prolog engine = new Prolog();
    private final int width;
    private final int height;
    private final Set<Vector2D> dirty = new HashSet<>();
    private final Collection<Consumer<VacuumEnvironment>> onInitListeners = new LinkedHashSet<>();
    private final Collection<Consumer<VacuumEnvironment>> onEnvironmentChangedListeners = new LinkedHashSet<>();
    private final Collection<Consumer<VacuumEnvironment>> onVacuumMovedListeners = new LinkedHashSet<>();

    private Vector2D vacuumPosition;
    private Orientation vacuumOrientation;

    public VacuumEnvironmentImpl(int width, int height, Vector2D vacuumPosition, Orientation orientation, Vector2D... dirty) {
        this.width = width;
        this.height = height;
        this.vacuumPosition = Objects.requireNonNull(vacuumPosition);
        if (!isInside(vacuumPosition)) {
            throw new IllegalArgumentException("Position " + vacuumPosition + " is out of the environment. Please use 1-based indexes");
        }
        this.vacuumOrientation = Objects.requireNonNull(orientation);
        this.dirty.addAll(Stream.of(dirty).collect(Collectors.toSet()));
    }

    @Override
    public void initialize() {
        this.onInit();
        this.onEnvironmentChanged();
        this.onVacuumMoved();
    }

    private void onInit() {
        onInitListeners.forEach(it -> it.accept(this));
    }

    @Override
    public void onInit(Consumer<VacuumEnvironment> listener) {
        onInitListeners.add(Objects.requireNonNull(listener));
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    public void cleanUp(Vector2D position) {
        if (!isInside(position)) {
            throw new IllegalArgumentException("Position " + position + " is out of the environment. Please use 1-based indexes");
        }
        dirty.remove(Objects.requireNonNull(position));
        this.onEnvironmentChanged();
    }

    public void cleanUp(int x, int y) {
        cleanUp(Vector2D.of(x, y));
    }

    public void setDirty(Vector2D position) {
        if (!isInside(position)) {
            throw new IllegalArgumentException("Position " + position + " is out of the environment. Please use 1-based indexes");
        }
        dirty.add(Objects.requireNonNull(position));
        this.onEnvironmentChanged();
    }

    public void setDirty(int x, int y) {
        setDirty(Vector2D.of(x, y));
    }

    public void setRandomlyDirty() {
        setDirty(Vector2D.randomIn(width, height));
    }

    public boolean isDirty(Vector2D position) {
        return dirty.contains(Objects.requireNonNull(position));
    }

    public boolean isDirty(int x, int y) {
        return dirty.contains(Vector2D.of(x, y));
    }

    public Vector2D getVacuumPosition() {
        return vacuumPosition;
    }

    public void setVacuumPosition(Vector2D vacuumPosition) {
        Vector2D previous = this.vacuumPosition;
        if (!isInside(vacuumPosition)) {
            throw new IllegalArgumentException("Position " + vacuumPosition + " is out of the environment. Please use 1-based indexes");
        }
        this.vacuumPosition = Objects.requireNonNull(vacuumPosition);
        if (!previous.equals(this.vacuumPosition)) {
            this.onEnvironmentChanged();
            this.onVacuumMoved();
        }
    }

    public void setVacuumPosition(int x, int y) {
        setVacuumPosition(Vector2D.of(x, y));
    }

    public Orientation getVacuumOrientation() {
        return vacuumOrientation;
    }

    public void setVacuumOrientation(Orientation vacuumOrientation) {
        Orientation previous = this.vacuumOrientation;
        this.vacuumOrientation = Objects.requireNonNull(vacuumOrientation);
        if (!previous.equals(this.vacuumOrientation)) {
            this.onEnvironmentChanged();
            this.onVacuumMoved();
        }
    }

    public boolean isInside(int x, int y) {
        return x >= 1 && x <= width
                && y >= 1 && y <= height;
    }

    public boolean isInside(Vector2D position) {
        return isInside(position.getX(), position.getY());
    }

    @Override
    public void onEnvironmentChanged(Consumer<VacuumEnvironment> listener) {
        onEnvironmentChangedListeners.add(Objects.requireNonNull(listener));
    }

    @Override
    public void onVacuumMoved(Consumer<VacuumEnvironment> listener) {
        onVacuumMovedListeners.add(Objects.requireNonNull(listener));
    }

    @Override
    public String toString() {
        StringBuilder envStr = new StringBuilder();
        for (int r = 0; r < height; r++) {
            for (int c = 0; c < width; c++) {
                if (getVacuumPosition().equals(Vector2D.of(c + 1, r + 1))) {
                    envStr.append(getVacuumOrientation().toString());
                } else if (isDirty(c + 1, r + 1)) {
                    envStr.append(DIRTY);
                } else {
                    envStr.append(EMPTY);
                }
            }
            envStr.append("\n");
        }
        return envStr.toString();
    }

    private void onEnvironmentChanged() {
        onEnvironmentChangedListeners.forEach(it -> it.accept(this));
    }

    private void onVacuumMoved() {
        onVacuumMovedListeners.forEach(it -> it.accept(this));
    }

    @Override
    public Prolog getProlog() {
        return engine;
    }
}
