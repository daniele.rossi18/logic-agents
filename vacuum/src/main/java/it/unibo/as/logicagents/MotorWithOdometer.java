package it.unibo.as.logicagents;

import it.unibo.as.logicagents.impl.MotorWithOdometerImpl;

/**
 * This is both a sensor and an actuator artifact.
 * It lets an agent mode awhile measuring how much it moved since the last it checked
 */
public interface MotorWithOdometer extends Artefact<VacuumEnvironment> {

    static MotorWithOdometer create(VacuumEnvironment environment) {
        return new MotorWithOdometerImpl(environment);
    }

    /**
     * Tries moving the agent toward a particular relative direction.
     * The agent may actually fail moving in case the requested movement would put it outside the environment.
     * In any case, this method returns <code>true</code>
     *
     * @param directionName is a {@link String} contaning the case-insensitive name of one value form the {@link Direction} ones
     * @return always <code>true</code>
     * @throws Exception
     */
    boolean move(String directionName) throws Exception;

    /**
     * Counts the amount of times the agent actually moved since the last invocation of this method
     * @return a non-negative <code>int</code>
     * @throws Exception
     */
    int countSteps() throws Exception;
}
