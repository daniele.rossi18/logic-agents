import alice.tuprolog.Prolog;
import alice.tuprolog.SolveInfo;
import alice.tuprolog.Theory;
import alice.tuprolog.exceptions.MalformedGoalException;
import alice.tuprolog.exceptions.NoMoreSolutionException;
import alice.tuprolog.exceptions.NoSolutionException;

import static alice.tuprolog.Terms.*;

public class TuProlog102 {
    public static void main(String[] args) {

        /*
         * This is an empty engine with some default pre-loaded libraries and operators.
         * Pre-loaded libraries: Prolog standard library + IO library + OOLibrary operators
         * Pre-loaded operators: standard prolog operators + OOLibrary operators
         *
         * Its theory is currently empty.
         */
        Prolog engine = new Prolog();

        /*
         * Let's parse a new theory
         */
        Theory theory = Theory.parseWithStandardOperators(
                "grandparent(GrandParent, Child) :- " +
                    "parent(GrandParent, Parent), " +
                    "parent(Parent, Child). "
        );

        /*
         * Let's set the parsed theory into the engine
         */
        engine.setTheory(theory);

        // Let's add some facts dynamically

        // one can assert facts through the theory manager ...
        engine.getTheoryManager().assertA(struct("parent", atom("a"), atom("b")), true, null, true);
        engine.getTheoryManager().assertZ(struct("parent", atom("a"), atom("c")), true, null, true);

        // ... or by issuing a query to the solver containing an assert goal
        try {
            engine.solve("assert(parent(b, d))");
        } catch (MalformedGoalException e) {
            // parsing may fail
            e.printStackTrace();
        }

        engine.solve(struct("assert", struct("parent", atom("c"), atom("e"))));

        // Let's perform some query:
        SolveInfo si;

        /*
         * Who's a grandchild of a?
         */
        try {
             si = engine.solve("grandparent(a, GrandChild)"); // this involves parsing
        } catch (MalformedGoalException e) {
            e.printStackTrace(); // parsing may fail

            // same query with no parsing
            // the query is built through the factory methods in alice.tuprolog.Terms.*
            si = engine.solve(struct("grandparent", atom("a"), var("GrandChild")));
        }

        // check if a solution exists
        while (si.isSuccess()) {
            try {
                System.out.println(si.getSolution()); // should print "grandparent(a,d)" first, then "grandparent(a,e)"
                System.out.println(si.getVarValue("GrandChild")); // should print "d" first, then "e"
            } catch (NoSolutionException e) {
                // solution may be missing in the general case
                e.printStackTrace();
            }

            try {
                // there may be more than one grandchild for a
                if (si.hasOpenAlternatives()) {
                    si = engine.solveNext();
                } else {
                    break;
                }
            } catch (NoMoreSolutionException e) {
                // next solution(s) may be missing in the general case
                break;
            }
        }
        System.out.println("no more grandchild for a");
    }
}
